#include <c.h>

#ifndef _ALL_FETCH_H_
#define _ALL_FETCH_H_

#include "postgres.h"
#include "fmgr.h"
#include "executor/spi.h"

#include "config.h"

typedef struct Data {
  int *column1;
  int *column2;
  uint32 size;
} Data;

Data *fetch_all (char *table, char *column1, char *column2);

Portal
fetch_batch_init (char *table, char *column1, char *column2, char *portal_name);

int fetch_batch(Portal portal, int batch_size, Data* output);
#endif //_ALL_FETCH_H_
