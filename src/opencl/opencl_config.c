#include "opencl_config.h"

static const char *SQL_KERNEL_QUERY = "SELECT KERNEL_LOCATION FROM OPENCL_CONFIG.OPENCL_KERNEL_LOCATION WHERE KERNEL_NAME='%s'";

int get_cl_info (CLInfo *cl_info)
{
  cl_int err;

  cl_platform_id platforms = NULL;
  cl_uint num_platforms;
  err = clGetPlatformIDs (1, &platforms, &num_platforms);
  if (err != CL_SUCCESS)
    {
      elog (ERROR, "Error: Failed to create platform id: %d\n", err);
    }

  cl_uint num_devices;
  cl_device_id devices[1];

  err = clGetDeviceIDs (platforms, CL_DEVICE_TYPE_GPU, 1, &devices[0], &num_devices);
  if (err != CL_SUCCESS)
    {
      elog (ERROR, "Error: Failed to create a device group: %d\n", err);
    }

  cl_uint max_compute_units;
  err = clGetDeviceInfo (devices[0], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof (cl_uint), &max_compute_units, NULL);
  if (err)
    {
      elog (ERROR, "Cannot read max compute units");
    }

  cl_uint dimensions;
  err = clGetDeviceInfo (devices[0], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof (cl_uint), &dimensions, NULL);
  if (err)
    {
      elog (ERROR, "Cannot read max dimension size");
    }
  //TODO Don;t know SEGFAULT
//  cl_uint max_grid[3];
//  err = clGetDeviceInfo (devices[0], CL_DEVICE_MAX_WORK_ITEM_SIZES,
//                         sizeof (size_t) * dimensions, max_grid, NULL);
//  if (err)
//    {
//      elog (ERROR, "Cannot read max work items");
//    }
//  elog (INFO, "OpenCL Device max grid size: %d x %d x %d", max_grid[0], max_grid[1], max_grid[2]);

  cl_uint max_work_group_size;
  err = clGetDeviceInfo (devices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof (size_t), &max_work_group_size, NULL);

  if (err != CL_SUCCESS)
    {
      elog (ERROR, "Cannot read CL_DEVICE_MAX_WORK_GROUP_SIZE");
    }

  cl_context context = NULL;
  context = clCreateContext (NULL, 1, devices, NULL, NULL, &err);
  if (!context)
    {
      printf ("Error: Failed to create a compute context: %d\n", err);
    }

  cl_command_queue commands = NULL;
  commands = clCreateCommandQueue (context, devices[0], 0, &err);
  if (!commands)
    {
      elog (ERROR, "Error: Failed to create a command commands: %d\n", err);
    }

  cl_info->command_queue = commands;
  cl_info->context = context;
  cl_info->max_work_group_size = max_work_group_size;
  cl_info->max_compute_units = max_compute_units;
  cl_info->device_id = devices[0];
  return 0;
}

char *get_kernel_location (char *kernel_name)
{
  bool isNull = false;
  int err_code = SPI_connect ();

  char *sql_query = psprintf (SQL_KERNEL_QUERY, kernel_name);
  if (err_code != SPI_OK_CONNECT)
    {
      ereport (ERROR, (errcode (ERRCODE_CONNECTION_FAILURE), errmsg ("Cannot connect to SPI, error code %d", err_code)));
      SPI_finish ();
    }

  err_code = SPI_execute (sql_query, true, 0);
  pfree (sql_query);
  if (err_code != SPI_OK_SELECT)
    {
      ereport (ERROR, (errcode (ERRCODE_CONNECTION_FAILURE), errmsg ("Cannot execute SQL: %s, error code %d", sql_query, err_code)));
      SPI_finish ();
    }

  if (SPI_processed == 0)
    {
      ereport (ERROR, (errcode (ERRCODE_CONNECTION_FAILURE), errmsg ("Didn't found kernel location under: %s kernel_name", kernel_name)));
      SPI_finish ();
    }

  SPITupleTable *spi_tuple_table = SPI_tuptable;
  TupleDesc spi_tuple_description = spi_tuple_table->tupdesc;

  HeapTuple tuple = spi_tuple_table->vals[0];
  Datum datumValue = SPI_getbinval (tuple, spi_tuple_description, 1, &isNull);
  VarChar *kernel_location = DatumGetVarCharP (datumValue);
  int size = VARSIZE (kernel_location) - VARHDRSZ;
  char *tmp = SPI_palloc (sizeof (char) * (size + 1));
  memcpy (tmp, kernel_location->vl_dat, size);
  tmp[size] = '\0';
  SPI_finish ();
  return tmp;
}
