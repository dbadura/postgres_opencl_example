#ifndef _ADD_OPENCL_H_
#define _ADD_OPENCL_H_

#include <CL/cl.h>
#include "../fetch/fetch.h"
#include "opencl/opencl_config.h"

typedef struct AddOpenCLContext {
  cl_mem column1_mem;
  cl_mem column2_mem;
  cl_mem output_mem;
  CLInfo *cl_info;
  cl_kernel kernel;
} AddOpenCLContext;

int init_call_context (CLInfo *cl_info, AddOpenCLContext *ctx, int size, char *kernel_location);
int destroy_call_context (AddOpenCLContext *ctx);

int add_opencl (AddOpenCLContext *ctx, Data *data, int *output);
#endif //_ADD_OPENCL_H_
