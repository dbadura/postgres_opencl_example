#include "add_cpu.h"

void add_cpu (Data* data, int *output) {
  for (uint row_id = 0; row_id < data->size; row_id++)
    {
      output[row_id] = data->column1[row_id] + data->column2[row_id];
    }
}
