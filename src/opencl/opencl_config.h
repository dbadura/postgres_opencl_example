#ifndef _OPENCL_CONFIG_H_
#define _OPENCL_CONFIG_H_

#include <CL/opencl.h>
#include <CL/cl.h>
#include "postgres.h"
#include "executor/spi.h"



typedef struct CLInfo {
  cl_device_id device_id;
  cl_device_type device_type;
  cl_context context;
  cl_command_queue command_queue;
  cl_uint max_work_group_size;
  cl_uint max_compute_units;
  
} CLInfo;

int get_cl_info(CLInfo* cl_info);

//TODO: Implement fetching kernel location

char* get_kernel_location(char* kernel_name);

#endif //_OPENCL_CONFIG_H_
