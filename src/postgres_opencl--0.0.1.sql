\echo Use "CREATE EXTENSION postgres_opencl" to load this file. \quit

CREATE OR REPLACE FUNCTION ADD_COLUMNS_CPU(table_name cstring, column1 cstring, column2 cstring)
    RETURNS TABLE
            (
                value INT
            )
AS
'MODULE_PATHNAME' LANGUAGE C STRICT;

CREATE OR REPLACE FUNCTION ADD_COLUMNS_BATCH_CPU(table_name cstring, column1 cstring,
                                                 column2 cstring, BATCH_SIZE int)
    RETURNS TABLE
            (
                value INT
            )
AS
'MODULE_PATHNAME' LANGUAGE C STRICT;

CREATE OR REPLACE FUNCTION ADD_COLUMNS_OPENCL(table_name cstring, column1 cstring, column2 cstring)
    RETURNS TABLE
            (
                value INT
            )
AS
'MODULE_PATHNAME' LANGUAGE C STRICT;

CREATE OR REPLACE FUNCTION ADD_COLUMNS_BATCH_OPENCL(table_name cstring, column1 cstring,
                                                    column2 cstring, BATCH_SIZE int)
    RETURNS TABLE
            (
                value INT
            )
AS
'MODULE_PATHNAME' LANGUAGE C STRICT;


CREATE SCHEMA OPENCL_CONFIG;
CREATE TABLE OPENCL_CONFIG.OPENCL_KERNEL_LOCATION
(
    KERNEL_NAME     VARCHAR(256),
    KERNEL_LOCATION VARCHAR(2048)
);

INSERT INTO OPENCL_CONFIG.OPENCL_KERNEL_LOCATION
VALUES ('add_2_columns', '/usr/lib/postgresql/10/lib/postgres_opencl_kernels/add_2_columns.cl');

GRANT ALL ON TABLE opencl_config.opencl_kernel_location TO postgres;
SELECT pg_catalog.pg_extension_config_dump('OPENCL_CONFIG.OPENCL_KERNEL_LOCATION', '');
