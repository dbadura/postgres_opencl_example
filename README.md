# Example PostgreSQL extension which uses OpenCL

# Requirements
* CMake 3.10
* PostgreSQL Headers, see how to install below
* OpenCL 1.2 compatible hardware
* Linux distribution (Ubuntu 18.04 was used)

## Features
This extension contains following examples:
* Fetch 2 columns from table and add them using OpenCL or CPU and return as table.
* Fetch 2 columns from table using batch fetching and add them using OpenCL or CPU and return as table.

This extension contains following SQL functios:
* ADD_COLUMNS_CPU(table_name, column1_name, column2_name)  - add 2 columns.
* ADD_COLUMNS_OPENCL(table_name, column1_name, column2_name) - add 2 columns using OpenCL.
* ADD_COLUMNS_BATCH_CPU(table_name, column1_name, column2_name, batch_size) - fetches data in batches and process every batch.
* ADD_COLUMNS_BATCH_OPENCL(table_name, column1_name, column2_name) - fetches data in batches and process every batch using OpenCL.

To configure OpenCL kernel location this extension creates table *OPENCL_KERNEL_LOCATION* in *OPENCL_CONFIG* schema.
It contains name of kernel and location on disk.

## Install OpenCL
For creating this example extension, AMD GPU and OpenCL in 1.2 version were used.
Install needed driver for your hardware vendor and check if OpenCL device is available in system using *clinfo*.

Install clinfo:
```bash
 sudo apt update && sudo apt install clinfo -y
```
 
Help page for AMD Radeon GPUs about installing driver: [help page](https://www.amd.com/en/support/kb/release-notes/amdgpu-installation).
 
## Install PostgreSQL
Install PostgreSQL with headers
```bash
sudo apt update && sudo apt install postgresql-10 postgresql-server-dev-10 libpq-dev postgresql-server-dev-all cmake
```

Headers files are located:
`/usr/include/postgresql/10/server`

## Install extension

```bash
mkdir build
cd build
cmake ../
cmake --build .
sudo make install
```

## Run regression tests
Run postgreSQL instance and then in `build` catalog run

```bash
export PGPASSWORD='your password for postgres user'
make installcheck
```

### How to write regression tests
In main CMakeLists.txt, in the following line:
```cmake
SET(LIST_OF_TESTS add_columns_10 add_columns_1000)
```

add name of your test. 
In `tests/postgres/sql` add `your-test.sql` with all SQLs which should be executed.
In `tests/postgres/test_data` add needed test data.
in `tests.postgres/expected` add `your-test.out` file with desired output.

## Run PostgreSQL as non-root user
Create folder `custom_config/configs/main` and copy config files from `/etc/postgresql/10/main`
Modify following keys in `postgresql.conf`:
* unix_socket_directories = 'path to `custom_config` dir'
* data_directory = 'path to `custom_config/data` dir'

Uncomment the following line: `ident_file = '/etc/postgresql/10/main/pg_ident.conf'`.

Run postgres with custom config:
```bash
/usr/lib/postgresql/10/bin/postgres -c config_file=`path to postgresql.conf`/postgresql.conf
```

Running PostgreSQL as non-root user is important to be able to debug extension.
## Debug extension
Enable ptrace, to be able to debug other process as non-root user:
```bash
echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
```

Connect to the running PostgreSQL and run the following query, to get background worker PID:
```sql
SELECT pg_backend_pid();
```

Attach `gdb` to the running process:
```bash
gdb postgres `PID returned from previous SQL`
```

## Helpful links:
* [PostgreSQL docs SPI](https://www.postgresql.org/docs/10/spi.html)
* [PostgreSQL docs extensions](https://www.postgresql.org/docs/10/extend-extensions.html)
