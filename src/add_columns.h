#ifndef _ADD_COLUMNS_H_
#define _ADD_COLUMNS_H_

#include "postgres.h"
#include "fmgr.h"
#include "utils/elog.h"
#include "utils/builtins.h"
#include "funcapi.h"
#include "executor/spi.h"
#include "access/htup_details.h"
#include "catalog/pg_type.h"
#include "lib/stringinfo.h"
#include "miscadmin.h"

#include "fetch/fetch.h"
#include "tuple_builder/builder.h"
#include "algorithm/add_cpu.h"
#include "algorithm/add_opencl.h"
#include "opencl/opencl_config.h"

Datum
add_columns_cpu (PG_FUNCTION_ARGS);

Datum
add_columns_batch (PG_FUNCTION_ARGS);

#endif //_ADD_COLUMNS_H_
