#include "add_columns.h"

PG_FUNCTION_INFO_V1 (add_columns_cpu);

static Data *init_data (uint32 batch_size)
{
  Data *data = palloc (sizeof (Data));
  data->column1 = palloc (sizeof (int32) * batch_size);
  data->column2 = palloc (sizeof (int32) * batch_size);
  data->size = batch_size;
  return data;
}

Datum
add_columns_cpu (PG_FUNCTION_ARGS)
{
  char *table = PG_GETARG_CSTRING (0);
  char *column1 = PG_GETARG_CSTRING (1);
  char *column2 = PG_GETARG_CSTRING (2);

  Data *data = fetch_all (table, column1, column2);
  int *result = palloc (sizeof (int) * data->size);

  add_cpu (data, result);
  build_return_tuples (fcinfo, result, data->size);
  PG_RETURN_INT32 (data->size);
}

PG_FUNCTION_INFO_V1 (add_columns_batch_cpu);

Datum
add_columns_batch_cpu (PG_FUNCTION_ARGS)
{
  char *table = PG_GETARG_CSTRING (0);
  char *column1 = PG_GETARG_CSTRING (1);
  char *column2 = PG_GETARG_CSTRING (2);
  int batch_size = PG_GETARG_INT32 (3);
  int total_rows = 0;
  int batch_rows;

  Portal portal = fetch_batch_init (table, column1, column2, "PORTAL");

  Data *data = init_data (batch_size);
  int *output = SPI_palloc (sizeof (int) * batch_size);

  batch_rows = fetch_batch (portal, batch_size, data);
  BuildCursor *cursor = init_build_cursor (fcinfo);
  while (0 != batch_rows)
    {
      add_cpu (data, output);
      build_return_tuples_batch (fcinfo, cursor, output, batch_rows);
      total_rows += batch_rows;
      batch_rows = fetch_batch (portal, batch_rows, data);
    }

  close_build_cursor (cursor);
  SPI_cursor_close (portal);
  PG_RETURN_INT32 (total_rows);
}

PG_FUNCTION_INFO_V1 (add_columns_opencl);

Datum
add_columns_opencl (PG_FUNCTION_ARGS)
{
  char *table = PG_GETARG_CSTRING (0);
  char *column1 = PG_GETARG_CSTRING (1);
  char *column2 = PG_GETARG_CSTRING (2);

  CLInfo *cl_info = palloc (sizeof (CLInfo));
  get_cl_info (cl_info);
  char *kernel_location = get_kernel_location ("add_2_columns");

  Data *data = fetch_all (table, column1, column2);

  AddOpenCLContext *ctx = palloc (sizeof (AddOpenCLContext));
  int err = init_call_context (cl_info, ctx, data->size, kernel_location);
  if (err != 0)
    {
      destroy_call_context (ctx);
      elog (ERROR, "init opencl context failed, error code: %d ", err);
    }

  int *output = palloc (sizeof (int) * data->size);
  err = add_opencl (ctx, data, output);
  if (err != 0)
    {
      destroy_call_context (ctx);
      elog (ERROR, "executing add opencl failed, error code: %d ", err);
    }
  destroy_call_context (ctx);
  build_return_tuples (fcinfo, output, data->size);
  PG_RETURN_INT32 (data->size);
}

PG_FUNCTION_INFO_V1 (add_columns_batch_opencl);

Datum
add_columns_batch_opencl (PG_FUNCTION_ARGS)
{
  char *table = PG_GETARG_CSTRING (0);
  char *column1 = PG_GETARG_CSTRING (1);
  char *column2 = PG_GETARG_CSTRING (2);
  int batch_size = PG_GETARG_INT32 (3);
  int total_rows = 0;
  int batch_rows;

  CLInfo *cl_info = palloc (sizeof (CLInfo));
  get_cl_info (cl_info);
  char *kernel_location = get_kernel_location ("add_2_columns");

  AddOpenCLContext *ctx = palloc (sizeof (AddOpenCLContext));
  int err = init_call_context (cl_info, ctx, batch_size, kernel_location);
  if (err != 0)
    {
      destroy_call_context (ctx);
      elog (ERROR, "init opencl context failed, error code: %d ", err);
    }

  Portal portal = fetch_batch_init (table, column1, column2, "PORTAL");
  BuildCursor *cursor = init_build_cursor (fcinfo);

  Data *data = init_data (batch_size);
  int *output = palloc (sizeof (int) * batch_size);

  batch_rows = fetch_batch (portal, batch_size, data);
  while (0 != batch_rows)
    {
      err = add_opencl (ctx, data, output);
      if (err != 0)
        {
          destroy_call_context (ctx);
          elog (ERROR, "executing add opencl failed, error code: %d ", err);
        }
      build_return_tuples_batch (fcinfo, cursor, output, batch_rows);
      total_rows += batch_rows;
      batch_rows = fetch_batch (portal, batch_rows, data);
    }

  destroy_call_context (ctx);
//  build_return_tuples (fcinfo, output, data->size);
  close_build_cursor (cursor);
  SPI_cursor_close (portal);
  PG_RETURN_INT32 (total_rows);
}
