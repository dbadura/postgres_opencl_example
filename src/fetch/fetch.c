#include "fetch.h"

Data *fetch_all (char *table, char *column1, char *column2)
{
  int execution_code = 0;
  Data *data = NULL;
  SPITupleTable *spi_tuple_table;
  TupleDesc spi_tuple_description;

  uint64 row_id = 0;
  uint64 amountOfTuples = 0;
  bool isNull = false;

  int err_code = SPI_connect ();
  if (err_code != SPI_OK_CONNECT)
    {
      ereport(ERROR, (errcode (ERRCODE_CONNECTION_FAILURE), errmsg ("Cannot connect to SPI, error code %d", err_code)));
    }

  char *sql_query = psprintf (SQL_QUERY, column1, column2, table);

  execution_code = SPI_execute (sql_query, true, 0);
  pfree (sql_query);

  if (execution_code != SPI_OK_SELECT)
    {
      ereport(
          ERROR,
          (errcode (ERRCODE_CONNECTION_FAILURE), errmsg ("Cannot execute SQL: %s, error code %d", sql_query, err_code)));
      SPI_finish ();
    }

  spi_tuple_table = SPI_tuptable;
  spi_tuple_description = spi_tuple_table->tupdesc;

  int column_amount = spi_tuple_description->natts;
  amountOfTuples = SPI_processed;
  data = SPI_palloc (sizeof (Data));
  data->size = amountOfTuples;

  data->column1 = SPI_palloc (sizeof (uint32) * amountOfTuples);
  data->column2 = SPI_palloc (sizeof (uint32) * amountOfTuples);

  int value = 0;
  // fetching all
  for (row_id = 0; row_id < amountOfTuples; row_id++)
    {
      HeapTuple tuple = spi_tuple_table->vals[row_id];
      Datum datumValue = SPI_getbinval (tuple, spi_tuple_description, 1, &isNull);
      value = DatumGetInt32 (datumValue);
      data->column1[row_id] = value;

      datumValue = SPI_getbinval (tuple, spi_tuple_description, 2, &isNull);
      value = DatumGetInt32 (datumValue);
      data->column2[row_id] = value;
    }

  SPI_finish ();
  return data;
}

Portal fetch_batch_init (char *table, char *column1, char *column2, char *portal_name)
{
  char *sql_query = psprintf (SQL_QUERY, column1, column2, table);

  int err_code = SPI_connect ();
  if (err_code != SPI_OK_CONNECT)
    {
      ereport(ERROR, (errcode (ERRCODE_CONNECTION_FAILURE), errmsg (
          "Cannot connect to SPI, error code %d", err_code)));
    }

  SPIPlanPtr execution_plan = SPI_prepare_cursor (sql_query, 0, NULL, 0);

  return SPI_cursor_open (portal_name, execution_plan, NULL, NULL, true);
}

int fetch_batch (Portal portal, int batch_size, Data *output)
{
  SPITupleTable *spi_tuple_table;
  TupleDesc spi_tuple_description;
  Datum value;
  bool isNull = false;

  SPI_cursor_fetch (portal, true, batch_size);
  spi_tuple_description = SPI_tuptable->tupdesc;
  spi_tuple_table = SPI_tuptable;

  for (uint64 row_id = 0; row_id < SPI_processed; row_id++)
    {
      HeapTuple tuple = spi_tuple_table->vals[row_id];
      Datum datumValue = SPI_getbinval (tuple, spi_tuple_description, 1, &isNull);
      value = DatumGetInt32 (datumValue);
      output->column1[row_id] = value;

      datumValue = SPI_getbinval (tuple, spi_tuple_description, 2, &isNull);
      value = DatumGetInt32 (datumValue);
      output->column2[row_id] = value;
    }

  output->size = SPI_processed;
  return SPI_processed;
}

