#include "add_opencl.h"

static const char *
readKernelSource (char *source_file)
{
  char *buffer = NULL;
  int string_size = 0, read_size = 0;
  FILE *handler = fopen (source_file, "r");

  if (handler)
    {
      fseek (handler, 0, SEEK_END);
      string_size = ftell (handler);
      //	Go to the begining
      rewind (handler);

      buffer = (char *) (malloc (sizeof (char) * (string_size + 1)));
      read_size = fread (buffer, sizeof (char), string_size, handler);
      buffer[string_size] = '\0';

      if (read_size != string_size)
        {
          // Something went wrong!
          free (buffer);
          buffer = NULL;
        }

    }
  else
    {
      elog (ERROR, "Cannot open file handler");
    }

  return buffer;
}

int init_call_context (CLInfo *cl_info, AddOpenCLContext *ctx, int size, char *kernel_location)
{
  cl_int err;
  const char *kernel_source = readKernelSource (kernel_location);
  if (!kernel_source)
    {
      printf ("Cannot read kernel source file\n");
      return EXIT_FAILURE;
    }
  cl_program program = clCreateProgramWithSource (cl_info->context, 1,
                                                  (const char **) &kernel_source, NULL, &err);
  if (!program)
    {
      printf ("Error: Failed to create compute program: %d\n", err);
      return EXIT_FAILURE;
    }

// Build the program executable
  err = clBuildProgram (program, 0, NULL, NULL, NULL, NULL);
  if (err != CL_SUCCESS)
    {
      size_t len;
      char buffer[2048];

      clGetProgramBuildInfo (program, cl_info->device_id, CL_PROGRAM_BUILD_LOG,
                             sizeof (buffer), buffer, &len);
      elog (ERROR, "Error: Failed to build program executable: %d\n. %s", err, buffer);
      exit (1);
    }

// Create the compute kernel in the program we wish to run
//
  cl_kernel kernel = clCreateKernel (program, "add", &err);
  if (!kernel || err != CL_SUCCESS)
    {
      printf ("Error: Failed to create compute kernel: %d\n", err);
      exit (1);
    }

// Create the input and output arrays in device memory for our calculation
//
  cl_mem column1_mem = clCreateBuffer (cl_info->context, CL_MEM_READ_ONLY, sizeof (float) * size,
                                       NULL, NULL);
  if (column1_mem == NULL)
    {
      printf ("Error: Failed to allocate device column 1 memory!\n");
      exit (1);
    }
  cl_mem column2_mem = clCreateBuffer (cl_info->context, CL_MEM_READ_ONLY, sizeof (float) * size,
                                       NULL, NULL);
  if (column2_mem == NULL)
    {
      printf ("Error: Failed to allocate device column 2 memory!\n");
      exit (1);
    }
  cl_mem output_mem = clCreateBuffer (cl_info->context, CL_MEM_WRITE_ONLY, sizeof (float) * size,
                                      NULL, NULL);
  if (output_mem == NULL)
    {
      printf ("Error: Failed to allocate device column output memory!\n");
      exit (1);
    }

  // Set the arguments to our compute kernel
//
  err = 0;
  err = clSetKernelArg (kernel, 0, sizeof (cl_mem), &column1_mem);
  err |= clSetKernelArg (kernel, 1, sizeof (cl_mem), &column2_mem);
  err |= clSetKernelArg (kernel, 2, sizeof (cl_mem), &output_mem);
  err |= clSetKernelArg (kernel, 3, sizeof (int), &size);
  if (err != CL_SUCCESS)
    {
      elog (WARNING, "Error: Failed to set kernel arguments! %d\n", err);
      err = clReleaseMemObject (column1_mem);
      if (err != CL_SUCCESS)
        {
          elog(ERROR, "Failed to release column1 memory");
        }

      err = clReleaseMemObject (column2_mem);
      if (err != CL_SUCCESS)
        {
          elog(ERROR, "Failed to release column2 memory");
        }
      err = clReleaseMemObject (output_mem);
      if (err != CL_SUCCESS)
        {
          elog(ERROR, "Failed to release output memory");
        }
      return 1;
    }

  ctx->column1_mem = column1_mem;
  ctx->column2_mem = column2_mem;
  ctx->output_mem = output_mem;
  ctx->cl_info = cl_info;
  ctx->kernel = kernel;
  return 0;
}

int add_opencl (AddOpenCLContext *ctx, Data *data, int *output)
{
  cl_int err = clEnqueueWriteBuffer (ctx->cl_info->command_queue, ctx->column1_mem, CL_TRUE, 0,
                                     sizeof (float) * data->size, data->column1, 0, NULL, NULL);
  if (err != CL_SUCCESS)
    {
      elog (WARNING, "Error: Failed to write to column1 memory!\n");
    }

  err = clEnqueueWriteBuffer (ctx->cl_info->command_queue, ctx->column2_mem, CL_TRUE, 0,
                              sizeof (float) * data->size, data->column2, 0, NULL, NULL);
  if (err != CL_SUCCESS)
    {
      elog (WARNING, "Error: Failed to write to column2 memory!\n");
    }

  size_t global = data->size;
  size_t local = data->size;
  if (ctx->cl_info->max_work_group_size < global) {
      local = ctx->cl_info->max_work_group_size;
    }
  err = clEnqueueNDRangeKernel (ctx->cl_info->command_queue, ctx
                                    ->kernel, 1, NULL, &global, &local, 0,
                                NULL,
                                NULL);
  if (err)
    {
      printf ("Error: Failed to execute kernel!\n");
      return EXIT_FAILURE;
    }

// Wait for the command commands to get serviced before reading back results
//
  clFinish (ctx->cl_info->command_queue);

// Read back the results from the device to verify the output
//
  err = clEnqueueReadBuffer (ctx->cl_info->command_queue, ctx->output_mem, CL_TRUE, 0,
                             sizeof (float) * data->size, output, 0, NULL, NULL);

  if (err != CL_SUCCESS)
    {
      printf ("Error: Failed to read output array! %d\n", err);
      exit (1);
    }
}

int destroy_call_context (AddOpenCLContext *ctx)
{
  cl_int err = clReleaseMemObject (ctx->column1_mem);
  if (err != CL_SUCCESS)
    {
      elog(ERROR, "Failed to release column1 memory");
    }

  err = clReleaseMemObject (ctx->column2_mem);
  if (err != CL_SUCCESS)
    {
      elog(ERROR, "Failed to release column2 memory");
    }
  err = clReleaseMemObject (ctx->output_mem);
  if (err != CL_SUCCESS)
    {
      elog(ERROR, "Failed to release output memory");
    }

  return err;
}
