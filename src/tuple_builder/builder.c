#include "builder.h"

void build_return_tuples (FunctionCallInfo fcinfo, int *values, uint32 size)
{
  BuildCursor *cursor = init_build_cursor (fcinfo);
  build_return_tuples_batch (fcinfo, cursor, values, size);
  close_build_cursor (cursor);
}

BuildCursor *init_build_cursor (FunctionCallInfo fcinfo)
{
  ReturnSetInfo *rsinfo = (ReturnSetInfo *) fcinfo->resultinfo;
  MemoryContext per_query_memory_context = rsinfo->econtext->ecxt_per_query_memory;
  BuildCursor *cursor = palloc (sizeof (BuildCursor));
  TupleDesc tuple_desc = CreateTemplateTupleDesc (1, false);
  TupleDescInitEntry (tuple_desc, (AttrNumber) 1, "a", INT4OID, -1, 0);
  cursor->tuple_desc = tuple_desc;
  cursor->old_memory_context = MemoryContextSwitchTo (per_query_memory_context);
  cursor->tuple_store = tuplestore_begin_heap (true, false, work_mem);
  return cursor;
}

void
build_return_tuples_batch (FunctionCallInfo fcinfo, BuildCursor *cursor, const int *values, uint32 size)
{
  bool isNull = false;
  Datum value;
  ReturnSetInfo *rsinfo = (ReturnSetInfo *) fcinfo->resultinfo;

  for (uint64 i = 0; i < size; i++)
    {
      rsinfo->returnMode = SFRM_Materialize;
      rsinfo->setResult = cursor->tuple_store;
      rsinfo->setDesc = cursor->tuple_desc;
      value = Int32GetDatum(values[i]);
      tuplestore_putvalues (cursor->tuple_store, cursor->tuple_desc, &value, &isNull);
    }
}

void close_build_cursor (BuildCursor *cursor)
{
  tuplestore_donestoring (cursor->tuple_store);
  MemoryContextSwitchTo (cursor->old_memory_context);
  pfree (cursor);
  SPI_finish ();
}
