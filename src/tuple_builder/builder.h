#ifndef _BUILDER_H_
#define _BUILDER_H_

#include "postgres.h"
#include "fmgr.h"
#include "utils/elog.h"
#include "utils/builtins.h"
#include "funcapi.h"
#include "executor/spi.h"
#include "catalog/pg_type.h"
#include "miscadmin.h"

typedef struct BuildCursor {
  MemoryContext old_memory_context;
  Tuplestorestate *tuple_store;
  TupleDesc tuple_desc;
} BuildCursor;
void build_return_tuples (FunctionCallInfo fcinfo, int *values, uint32 size);

void
build_return_tuples_batch (FunctionCallInfo fcinfo, BuildCursor *cursor, const int *values, uint32 size);

BuildCursor *init_build_cursor (FunctionCallInfo fcinfo);

void close_build_cursor (BuildCursor *cursor);
#endif //_BUILDER_H_
